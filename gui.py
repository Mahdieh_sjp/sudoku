def print_table(elements: list):
    print(
        chr(0x250C)
        + 7 * chr(0x2500)
        + chr(0x252C)
        + 7 * chr(0x2500)
        + chr(0x252C)
        + 7 * chr(0x2500)
        + chr(0x2510)
    )
    for i in range(9):
        if i == 3 or i == 6:
            print(
                chr(0x251C)
                + 7 * chr(0x2500)
                + chr(0x253C)
                + 7 * chr(0x2500)
                + chr(0x253C)
                + 7 * chr(0x2500)
                + chr(0x2524)
            )
        for j in range(9):
            if j % 3 == 0:
                print(chr(0x2502), end=" ")
            if elements[i][j] == 0:
                print("_", end=" ")
            else:
                print(elements[i][j], end=" ")
        print(chr(0x2502))
    print(
        chr(0x2514)
        + 7 * chr(0x2500)
        + chr(0x2534)
        + 7 * chr(0x2500)
        + chr(0x2534)
        + 7 * chr(0x2500)
        + chr(0x2518)
    )

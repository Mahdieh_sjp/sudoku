import json
import random as rnd
import math
from gui import print_table
import copy


class PossibleSolution(object):
    def __init__(self):
        self.table = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
        ]
        self.fitness = None

    def calculate_fitness(self):
        # I wrote a fittness function but it didn't work well.
        # This function you see here is somehow copied.

        row_count = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        column_count = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        block_count = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        row_sum = 0
        column_sum = 0
        block_sum = 0

        for i in range(9):
            for j in range(9):
                # row_count[self.table[i][j] - 1] = 1
                row_count[self.table[i][j] - 1] += 1

            row_sum += (1.0 / len(set(row_count))) / 9
            # row_sum += 9 - sum(row_count)
            row_count = [0, 0, 0, 0, 0, 0, 0, 0, 0]

        for i in range(9):
            for j in range(9):
                column_count[self.table[j][i] - 1] += 1
                # column_count[self.table[j][i] - 1] = 1

            column_sum += (1.0 / len(set(column_count))) / 9
            # column_sum += 9 - sum(column_count)
            column_count = [0, 0, 0, 0, 0, 0, 0, 0, 0]

        for i in range(0, 9, 3):
            for j in range(0, 9, 3):
                block_count[self.table[i][j] - 1] += 1
                block_count[self.table[i][j + 1] - 1] += 1
                block_count[self.table[i][j + 2] - 1] += 1

                block_count[self.table[i + 1][j] - 1] += 1
                block_count[self.table[i + 1][j + 1] - 1] += 1
                block_count[self.table[i + 1][j + 2] - 1] += 1

                block_count[self.table[i + 2][j] - 1] += 1
                block_count[self.table[i + 2][j + 1] - 1] += 1
                block_count[self.table[i + 2][j + 2] - 1] += 1

                block_sum += (1.0 / len(set(block_count))) / 9
                # block_count[self.table[i][j] - 1] = 1
                # block_count[self.table[i][j + 1] - 1] = 1
                # block_count[self.table[i][j + 2] - 1] = 1

                # block_count[self.table[i + 1][j] - 1] = 1
                # block_count[self.table[i + 1][j + 1] - 1] = 1
                # block_count[self.table[i + 1][j + 2] - 1] = 1

                # block_count[self.table[i + 2][j] - 1] = 1
                # block_count[self.table[i + 2][j + 1] - 1] = 1
                # block_count[self.table[i + 2][j + 2] - 1] = 1

                # block_sum += 9 - sum(block_count)
                block_count = [0, 0, 0, 0, 0, 0, 0, 0, 0]

        # if int(row_sum) == 0 and int(column_sum) == 0 and int(block_sum) == 0:
        #     fitness = 0
        # else:
        #     fitness = column_sum + block_sum + row_sum
        if int(row_sum) == 1 and int(column_sum) == 1 and int(block_sum) == 1:
            fitness = 1.0
        else:
            fitness = column_sum * block_sum
        self.fitness = fitness

    def mutate(self, table):
        r = rnd.uniform(0, 1.0)
        mutated = False

        if r < 0.05:
            while not mutated:
                row = rnd.randint(0, 8)
                first_idx = rnd.randint(0, 8)
                second_idx = rnd.randint(0, 8)
                while first_idx == second_idx:
                    first_idx = rnd.randint(0, 8)
                    second_idx = rnd.randint(0, 8)
                if (
                    not table.does_exist_in_col(second_idx, self.table[row][first_idx])
                    and not table.does_exist_in_col(
                        first_idx, self.table[row][second_idx]
                    )
                    and not table.does_exist_in_block(
                        row, second_idx, self.table[row][first_idx]
                    )
                    and not table.does_exist_in_block(
                        row, first_idx, self.table[row][second_idx]
                    )
                ):
                    temp = self.table[row][second_idx]
                    self.table[row][second_idx] = self.table[row][first_idx]
                    self.table[row][first_idx] = temp
                    mutated = True
        return mutated


class Table(PossibleSolution):
    def __init__(self, table):
        self.known_values = table

    def does_exist_in_row(self, row, value):
        for col in range(9):
            if self.known_values[row][col] == value:
                return True
        return False

    def does_exist_in_col(self, col, value):
        for row in range(9):
            if self.known_values[row][col] == value:
                return True
        return False

    def does_exist_in_block(self, row, col, value):
        i = 3 * math.floor(row / 3)
        j = 3 * math.floor(col / 3)
        if (
            (self.known_values[i][j] == value)
            or (self.known_values[i][j + 1] == value)
            or (self.known_values[i][j + 2] == value)
            or (self.known_values[i + 1][j] == value)
            or (self.known_values[i + 1][j + 1] == value)
            or (self.known_values[i + 1][j + 2] == value)
            or (self.known_values[i + 2][j] == value)
            or (self.known_values[i + 2][j + 1] == value)
            or (self.known_values[i + 2][j + 2] == value)
        ):
            return True
        else:
            return False


class PossibleSolutions(object):
    def __init__(self):
        self.solutions = []

    def start(self, base: Table):
        self.solutions = []

        choices = PossibleSolution()
        choices.table = [[[] for j in range(9)] for i in range(9)]
        for row in range(9):
            for col in range(9):
                for value in range(9):
                    if base.known_values[row][col] != 0:
                        choices.table[row][col].append(base.known_values[row][col])
                        break
                    if (base.known_values[row][col] == 0) and not (
                        base.does_exist_in_col(col, value + 1)
                        or base.does_exist_in_row(row, value + 1)
                        or base.does_exist_in_block(row, col, value + 1)
                    ):
                        choices.table[row][col].append(value + 1)

        for _ in range(1000):
            new_sol = PossibleSolution()
            for i in range(9):
                row = [0, 0, 0, 0, 0, 0, 0, 0, 0]
                for j in range(9):
                    if base.known_values[i][j] != 0:
                        row[j] = base.known_values[i][j]
                    elif base.known_values[i][j] == 0:
                        row[j] = choices.table[i][j][
                            rnd.randint(0, len(choices.table[i][j]) - 1)
                        ]

                while len(list(set(row))) != 9:
                    for j in range(9):
                        if base.known_values[i][j] == 0:
                            row[j] = choices.table[i][j][
                                rnd.randint(0, len(choices.table[i][j]) - 1)
                            ]

                new_sol.table[i] = row

            self.solutions.append(new_sol)

        self.calculate_fitness()

    def calculate_fitness(self):
        for sol in self.solutions:
            sol.calculate_fitness()

    def sort(self):
        self.solutions.sort(key=lambda x: x.fitness)


class AI(object):
    def __init__(self):
        self.known_values = None
        self.population = None

    def print_solved_puzzle(self, solution):
        print_table(solution)

    def solve(self, problem):
        problem_data = json.loads(problem)
        self.known_values = Table(problem_data)
        print_table(self.known_values.known_values)

        print("Intializing population...")
        self.population = PossibleSolutions()
        self.population.start(self.known_values)

        for g in range(2000):
            print("Generation {}".format(g))
            for i in range(1000):
                if self.population.solutions[i].fitness == 1:
                    print("Solution found at generation {}!".format(g))
                    self.print_solved_puzzle(self.population.solutions[i].table)
                    return self.population.solutions[i].table  # returns the solution

            self.population.solutions = self.create_next_population()
            self.population.calculate_fitness()
            self.population.sort()

        print("No solution found")

    def pick_parent(self, solutions):
        option1 = solutions[rnd.randint(0, (len(solutions) - 1))]
        option2 = solutions[rnd.randint(0, (len(solutions) - 1))]
        fit1 = option1.fitness
        fit2 = option2.fitness

        if fit1 > fit2:
            fitter = option1
            weaker = option2
        else:
            fitter = option2
            weaker = option1

        r = rnd.uniform(0, 1.0)

        if r < 0.85:
            return fitter
        return weaker

    def create_next_population(self):
        new_population = []

        self.population.sort()
        elites = []
        for i in range(20):
            elite = PossibleSolution()
            elite.table = copy.deepcopy(self.population.solutions[i].table)
            elites.append(elite)
        for _ in range(20, 1000, 2):
            parent1 = self.pick_parent(self.population.solutions)
            parent2 = self.pick_parent(self.population.solutions)

            child1, child2 = self.crossover(parent1, parent2)

            child1.mutate(self.known_values)
            child1.calculate_fitness()

            child2.mutate(self.known_values)
            child2.calculate_fitness()

            new_population.append(child1)
            new_population.append(child2)

        for i in range(20):
            new_population.append(elites[i])
        return new_population

    def crossover(self, parent1: PossibleSolution, parent2: PossibleSolution):
        child1 = PossibleSolution()
        child2 = PossibleSolution()

        child1.table = copy.deepcopy(parent1.table)
        child2.table = copy.deepcopy(parent2.table)

        crossover_point = rnd.randint(1, 8)

        for i in range(crossover_point):
            child1.table[i], child2.table[i] = child2.table[i], child1.table[i]

        return child1, child2


t = """
        [
            [1 , 0 , 4 , 8 , 6 , 5 , 2 , 3 , 7],
            [7 , 0 , 5 , 4 , 1 , 2 , 9 , 6 , 8],
            [8 , 0 , 2 , 3 , 9 , 7 , 1 , 4 , 5],
            [9 , 0 , 1 , 7 , 4 , 8 , 3 , 5 , 6],
            [6 , 0 , 8 , 5 , 3 , 1 , 4 , 2 , 9],
            [4 , 0 , 3 , 9 , 2 , 6 , 8 , 7 , 1],
            [3 , 0 , 9 , 6 , 5 , 4 , 7 , 1 , 2],
            [2 , 0 , 6 , 1 , 7 , 9 , 5 , 8 , 3],
            [5 , 0 , 7 , 2 , 8 , 3 , 6 , 9 , 4]
        ]
    """

ai = AI()
solution = ai.solve(t)
